const cvsContext = $('#gameCanvas')[0].getContext('2d')
cvsWidth = 600
cvsHeight = 600
radius = 5
directions = {
  up: 'UP',
  down: 'DOWN',
  left: 'LEFT',
  right: 'RIGHT'
}
frameCount = 0

class Point {
  constructor(x = 0, y = 0) {
    this.x = x
    this.y = y
  }

  isEqualTo(point) {
    if (this.x == point.x && this.y == point.y) {
      return true
    } else {
      return false
    }
  }
}

class Food {
  constructor(position = new Point()) {
    this.position = position
  }

  draw() {
    cvsContext.beginPath()
    cvsContext.fillStyle = 'red'
    cvsContext.arc(this.position.x, this.position.y, radius, 0, 2 * Math.PI)
    cvsContext.fill()
    cvsContext.closePath()
  }

  regen() {
    this.position.x = Math.random() * cvsWidth
    this.position.y = Math.random() * cvsHeight

    return this
  }

  isEaten(headPos) {
    let distanceX = this.position.x - headPos.x;
    let distanceY = this.position.y - headPos.y;

    if (Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2)) <= 2 * radius) {
      this.regen()
      return true
    }
    return false
  }
}

class Snake {
  constructor(position = [new Point(cvsWidth / 2, cvsHeight / 2)]) {
    this.position = position
    this.curDirection = 'UP'
  }

  draw() {
    cvsContext.fillStyle = 'black'
    this.position.forEach(pos => {
      cvsContext.beginPath()
      cvsContext.arc(pos.x, pos.y, radius, 0, 2 * Math.PI)
      cvsContext.fill()
      cvsContext.closePath()
    });
  }

  manageBounds() {
    if (this.position[0].x < 0) this.position[0].x = cvsWidth - 10;
    if (this.position[0].x > cvsWidth) this.position[0].x = 10;
    if (this.position[0].y < 0) this.position[0].y = cvsHeight - 10;
    if (this.position[0].y > cvsHeight) this.position[0].y = 10;
  }

  update() {
    let foodEaten = false
    if (frameCount % 6 == 0) {
      if (foodEaten == true) {
        this.position.push(this.position[this.position.length - 1])
        foodEaten = false
      }
      // if (foodObj.isEaten(this.position[0])) {
      //   this.position.push(this.position[this.position.length - 1])
      // }

      this.manageBounds()

      for (let index = this.position.length - 1; index > 0; index--) {
        // console.log(this.position, this.position.length, index)
        if (this.position[index].isEqualTo(this.position[0]) && this.position.length > 2) {
          this.position.splice(1)
          break
        }

        this.position[index] = this.position[index - 1]
      }

      switch (this.curDirection) {
        case directions.up:
          this.position[0].y -= radius * 2
          break;
        case directions.down:
          this.position[0].y += radius * 2
          break;
        case directions.left:
          this.position[0].x -= radius * 2
          break;
        case directions.right:
          this.position[0].x += radius * 2
          break;
        default:
          break;
      }

      if (foodObj.isEaten(this.position[0])) {
        foodEaten = true
      }
    }
  }
}

foodObj = new Food().regen()
snakeObj = new Snake()

function controller() {
  cvsContext.clearRect(0, 0, cvsWidth, cvsHeight)
  foodObj.draw()
  snakeObj.update()
  snakeObj.draw()

  frameCount++;

  requestAnimationFrame(controller)
}

$(document).bind('keydown', function (evt) {
  switch (evt.keyCode) {
    case 37:
      if (snakeObj.curDirection != directions.left && snakeObj.curDirection != directions.right) snakeObj.curDirection = directions.left;
      break;
    case 38:
      if (snakeObj.curDirection != directions.up && snakeObj.curDirection != directions.down) snakeObj.curDirection = directions.up;
      break;
    case 39:
      if (snakeObj.curDirection != directions.right && snakeObj.curDirection != directions.left) snakeObj.curDirection = directions.right;
      break;
    case 40:
      if (snakeObj.curDirection != directions.down && snakeObj.curDirection != directions.up) snakeObj.curDirection = directions.down;
      break;
  }
});

requestAnimationFrame(controller)